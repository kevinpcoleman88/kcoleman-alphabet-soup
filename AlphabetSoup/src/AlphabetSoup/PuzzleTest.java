package AlphabetSoup;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

public class PuzzleTest {

	private static final ArrayList<String> WORDS_ONE = new ArrayList<String>(Arrays.asList("ABC", "AEI"));
	private static final char[][] PUZZLE_ONE = { 
			{ 'A', 'B', 'C' }, 
			{ 'D', 'E', 'F' }, 
			{ 'G', 'H', 'I' } 
		};

	private static final ArrayList<String> WORDS_TWO = new ArrayList<String>(Arrays.asList("HELLO", "GOOD", "BYE"));
	private static final char[][] PUZZLE_TWO = { 
			{ 'H', 'A', 'S', 'D', 'F' }, 
			{ 'G', 'E', 'Y', 'B', 'H' },
			{ 'J', 'K', 'L', 'Z', 'X' }, 
			{ 'C', 'V', 'B', 'L', 'N' }, 
			{ 'G', 'O', 'O', 'D', 'O' } 
		};
	
	private static final ArrayList<String> WORDS_THREE = new ArrayList<String>(
			Arrays.asList("ALPHABET SOUP", "ENLIGHTEN", "EITC CORP", "SOLUTION", "JAVA", "PUZZLE"));
	private static final char[][] PUZZLE_THREE = { 
			{ 'H', 'E', 'S', 'D', 'F', 'N', 'Q', 'Z', 'R', 'W', 'I', 'D' },
			{ 'A', 'N', 'Y', 'B', 'H', 'S', 'G', 'D', 'C', 'E', 'K', 'A' },
			{ 'J', 'L', 'L', 'Z', 'X', 'N', 'A', 'Q', 'N', 'R', 'Y', 'W' },
			{ 'C', 'I', 'P', 'U', 'Z', 'Z', 'L', 'E', 'J', 'D', 'R', 'B' },
			{ 'G', 'G', 'O', 'H', 'O', 'J', 'K', 'Z', 'O', 'N', 'P', 'B' },
			{ 'H', 'H', 'S', 'D', 'A', 'H', 'S', 'Q', 'U', 'O', 'K', 'X' },
			{ 'G', 'T', 'Y', 'V', 'H', 'B', 'B', 'Q', 'Y', 'I', 'M', 'Z' },
			{ 'J', 'E', 'A', 'Z', 'X', 'Y', 'E', 'L', 'A', 'T', 'R', 'I' },
			{ 'C', 'N', 'B', 'L', 'N', 'C', 'Z', 'T', 'Q', 'U', 'E', 'R' },
			{ 'G', 'O', 'O', 'D', 'O', 'F', 'K', 'A', 'S', 'L', 'V', 'Y' },
			{ 'H', 'A', 'S', 'D', 'E', 'I', 'T', 'C', 'C', 'O', 'R', 'P' },
			{ 'G', 'E', 'Y', 'B', 'H', 'C', 'O', 'A', 'T', 'S', 'U', 'U' },
			{ 'J', 'K', 'L', 'Z', 'X', 'V', 'M', 'R', 'Q', 'A', 'E', 'P' }, 
		};

	@Test
	public void testPuzzleSolverOne() {
		String ans = "", expected = "ABC 0:0 0:2 AEI 0:0 2:2 ";
		for (String word : WORDS_ONE) {
			ans += PuzzleSolver.findWord(PUZZLE_ONE, word);
		}
		assertEquals(ans, expected);
	}

	@Test
	public void testPuzzleSolverTwo() {
		String ans = "", expected = "ABC 0:0 0:2 AEI 0:0 2:2 ";
		for (String word : WORDS_ONE) {
			ans += PuzzleSolver.findWord(PUZZLE_ONE, word);
		}
		assertEquals(ans, expected);
	}

	@Test
	public void testPuzzleSolveThree() {
		String ans = "", expected = "ALPHABET SOUP 1:0 12:11 ENLIGHTEN 0:1 8:1 EITC CORP 10:4 10:11 SOLUTION 11:9 4:9 JAVA 4:5 7:2 PUZZLE 3:2 3:7 ";

		for (String word : WORDS_THREE) {
			ans += PuzzleSolver.findWord(PUZZLE_THREE, word);
		}
		assertEquals(ans, expected);

	}
}
