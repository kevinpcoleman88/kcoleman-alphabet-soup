package AlphabetSoup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PuzzleSolver {

	public static String findWord(char[][] puzzle, String word) {
		String shortWord = word.replaceAll("\\s", "");
		String endCoors = null;
		for (int row = 0; row < puzzle.length; row++) {
			for (int col = 0; col < puzzle[row].length; col++) {
				if((endCoors = findWordHelper("u", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("d", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("l", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("r", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("ul", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("ur", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("dl", shortWord, puzzle, row, col)) != null
						|| (endCoors = findWordHelper("dr", shortWord, puzzle, row, col)) != null) {
					return word + " " + row + ":" + col + " " + endCoors + " ";
				}
			}
		}
		return null;
	}
	/**
	 * Using recursion, iterate over the charaters in the targetWord, using the
	 * chosen direction until the end of the targetWord is reached.
	 * 
	 * @param direction
	 * @param targetWord
	 * @param puzzle
	 * @param row
	 * @param col
	 * @return
	 */
	public static String findWordHelper(String direction, String targetWord, char[][] puzzle, int row, int col) {
		// Out of bounds check
		if (row < 0 || row == puzzle.length || col < 0 || col == puzzle[row].length || puzzle[row][col] != targetWord.charAt(0))
			return null;

		// Check if current letter matches and if so, recurse
		if (puzzle[row][col] == targetWord.charAt(0)) {
			// End of the word was reached. Word matched.
			if (targetWord.length() == 1) return "" + row + ":" + col;

			switch (direction) {
			case "u":
				return findWordHelper("u", targetWord.substring(1), puzzle, row, col - 1);
			case "d":
				return findWordHelper("d", targetWord.substring(1), puzzle, row, col + 1);
			case "l":
				return findWordHelper("l", targetWord.substring(1), puzzle, row - 1, col);
			case "r":
				return findWordHelper("r", targetWord.substring(1), puzzle, row + 1, col);
			case "ul":
				return findWordHelper("ul", targetWord.substring(1), puzzle, row - 1, col - 1);
			case "ur":
				return findWordHelper("ur", targetWord.substring(1), puzzle, row + 1, col - 1);
			case "dl":
				return findWordHelper("dl", targetWord.substring(1), puzzle, row - 1, col + 1);
			case "dr":
				return findWordHelper("dr", targetWord.substring(1), puzzle, row + 1, col + 1);
			}
		}

		// Word not found.
		return null;
	}

	public static void main(String[] args) {
		char[][] puzzle;
		ArrayList<String> words = new ArrayList<String>();
		String currLine = "";
		int rows = 0, cols = 0;

		System.out.println("Welcome to the Alphabet Soup Solver!\nPlease paste the formatted Crossword Puzzle in the AlphabetSoup folder.\n\nType in the name of the Puzzle file and press Enter.");
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			currLine = reader.readLine();
			
			// Read in crossword puzzle
			reader = new BufferedReader(new FileReader(new File(currLine)));
			
			//Read in Dimensions
			currLine = reader.readLine();
			rows = Integer.parseInt(currLine.split("x")[0].trim());
			cols = Integer.parseInt(currLine.split("x")[1].trim());
			//System.out.println("Rows: " + rows + ", Columns: " + cols);

			//Read in puzzle
			puzzle = new char[rows][cols];
			for(int row = 0; row < rows; row++) {
				String line = reader.readLine();
				for(int col = 0; col < cols; col++) {
					puzzle[row][col] = line.charAt(col * 2);
				}
			}
			
			//Read in vocab words
			while ((currLine = reader.readLine()) != null){
				words.add(currLine.trim());
			} 

			//Iterate over words and print solutions to console
			for(String word : words) {
				String key = "";
				key = findWord(puzzle, word);
				if(key != null) System.out.println(key);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
